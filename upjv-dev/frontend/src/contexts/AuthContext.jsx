import React, { createContext, useContext, useEffect } from 'react'
import { node } from 'prop-types'
import { useNavigate } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { clearStore, loginUserSuccess } from 'store/reducers/auth'

const AuthContext = createContext({})

export const AuthContextProvider = ({ children }) => {
  const dispatch = useDispatch()
  const { data: user, error: authError } = useSelector(state => state.auth)
  const localUser = JSON.parse(localStorage.getItem('local-user'))

  const loggedInUser = user || {}
  const isAdmin = loggedInUser.role === 'admin'

  const navigate = useNavigate()

  const handleLogout = () => {
    localStorage.removeItem('local-user')
    dispatch(clearStore())
    navigate('/login')
  }

  useEffect(() => {
    if (user) {
      navigate('/main/modules')
    } else if (localUser) {
      dispatch(loginUserSuccess(localUser))
      navigate('/main/modules')
    } else {
      navigate('/login')
    }
  }, [user])

  return (
    <AuthContext.Provider value={{ user: { ...loggedInUser, isAdmin }, authError, handleLogout }}>
      {children}
    </AuthContext.Provider>
  )
}

export const useAuthContext = () => useContext(AuthContext)

AuthContextProvider.propTypes = {
  children: node.isRequired
}

export default useAuthContext
