import React, { createContext, useContext, useEffect, useState } from 'react'
import { node } from 'prop-types'
import { useSelector, useDispatch } from 'react-redux'
import { Alert, Snackbar } from '@mui/material'

import { fetchModules } from '../store/actions/modules'
import { fetchProfessors } from '../store/actions/professors'
import { fetchUCRules } from '../store/actions/ucRules'
import { fetchEducations } from '../store/actions/educations'
import useAuthContext from './AuthContext'

const AppContext = createContext({})

export const AppContextProvider = ({ children }) => {
  const [notifOpen, toggleNotif] = useState(false)
  const [currentProfessor, setCurrentProfessor] = useState(null)
  const [notif, setNotif] = useState({ type: 'success', message: 'Succes' })

  const dispatch = useDispatch()
  const { user: { userId, isAdmin, role } } = useAuthContext()
  const { data: user } = useSelector(state => state.auth)
  const { data: modules } = useSelector(state => state.modules)
  const { data: professors } = useSelector(state => state.professors)
  const { data: ucRules } = useSelector(state => state.ucRules)
  const { data: educations } = useSelector(state => state.educations)

  const getAdminData = () => {
    fetchModules()(dispatch)
    fetchEducations()(dispatch)
    fetchProfessors()(dispatch)
    fetchUCRules()(dispatch)
  }

  const getProfessorData = () => {
    fetchModules()(dispatch)
    fetchProfessors()(dispatch)
    fetchEducations()(dispatch)
  }

  const refreshData = () => {
    if (user.token) {
      if (isAdmin) getAdminData()
      else { getProfessorData() }
    }
  }

  const showNotif = (data) => {
    toggleNotif(true)
    setNotif(data)
    setTimeout(() => toggleNotif(false), 5000)
  }

  useEffect(() => { if (user) refreshData() }, [user])
  useEffect(() => {
    if (professors.length && role === 'professor') {
      const profInfo = professors.find(prof => prof.user === userId)
      setCurrentProfessor(profInfo)
    }
  }, [user, professors])

  return (
    <AppContext.Provider value={{ refreshData, showNotif, currentProfessor, modules, educations, professors, ucRules }}>
    <Snackbar open={notifOpen} autoHideDuration={4000} anchorOrigin={{ vertical: 'top', horizontal: 'right' }}>
      <Alert severity={notif.type}>{notif.message}</Alert>
    </Snackbar>
      {children}
    </AppContext.Provider>
  )
}

export const useAppContext = () => useContext(AppContext)

AppContextProvider.propTypes = {
  children: node.isRequired
}

export default useAppContext
