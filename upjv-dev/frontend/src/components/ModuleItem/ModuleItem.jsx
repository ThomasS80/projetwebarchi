/* eslint-disable react/prop-types */
import React from 'react'
import { Chip, Grid, Paper } from '@mui/material'
import styled from 'styled-components'
import Progress from '../Progress/Progress'
import { getModuleAssignedGroupsRatio } from '../../shared/utils'

const TitleWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`

const ModuleCard = styled(Paper)`
  width: 100%;
  padding: 12px;
  cursor: pointer;
  &:hover { background: ${({ minimal }) => minimal ? 'inherit' : '#d6d3d3'};};
`

const getStatusColor = (status) => {
  const formattedStatus = status.toLowerCase()
  switch (formattedStatus) {
    case 'obligatoire': return 'error'
    case 'optionnelle': return 'success'
    case 'optionnel': return 'success'
    default: return 'warning'
  }
}

export default function ModuleItem ({ module, minimal }) {
  const { ratio, totalGroups, totalAssignedGroups } = getModuleAssignedGroupsRatio(module)

  return (
    <ModuleCard elevation={3} minimal={minimal}>
      <TitleWrapper>
      <h5 style={{ borderLeft: '2px solid grey', paddingLeft: '5px', color: '#00345f', width: '33%' }}>{module.title}<span style={{ color: '#aaa', marginLeft: '5px' }}>({module.ref})</span></h5>
      <Chip label={module.status || 'N/A'} color={getStatusColor(module.status)} size="small" />
      </TitleWrapper>
      <Grid container spacing={1}>
      <Grid item lg={3}>
        <span><b>Total groupes:</b><span>{` ${totalGroups}`}</span></span><br/>
        <span><b>Effectif:</b><span>{` ${module.totalStudents}`}</span></span>
      </Grid>
      <Grid item lg={2}>
        <b>Groupes CM:</b><span>{` ${module.CM.totalGroups}`}</span><br/>
        <b>Heures CM:</b><span>{` ${module.CM.totalHours}hrs`}</span>
      </Grid>
      <Grid item lg={2}>
        <b>Groupes TD:</b><span>{` ${module.TD.totalGroups}`}</span><br/>
        <b>Heures TD:</b><span>{` ${module.TD.totalHours}hrs`}</span>
      </Grid>
      <Grid item lg={2}>
        <b>Groupes TP:</b><span>{` ${module.TP.totalGroups}`}</span><br/>
        <b>Heures TP:</b><span>{` ${module.TP.totalHours}hrs`}</span>
      </Grid>
      <Grid item lg={2} sx={{ display: 'flex', justifyContent: 'flex-end' }}>
        <Progress value={ ratio * 100} title={`${totalAssignedGroups} groupes assurés`} />
      </Grid>
    </Grid>
    </ModuleCard>
  )
}
