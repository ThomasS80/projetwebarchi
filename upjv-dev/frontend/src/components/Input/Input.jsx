/* eslint-disable react/prop-types */
import React from 'react'
import { TextField } from '@mui/material'

export default function Input ({ register, field, error, ...rest }) {
  return (
    <TextField {...register(field)} error={!!error} helperText={error ? error.message : ''} size='small' fullWidth {...rest}/>
  )
}
