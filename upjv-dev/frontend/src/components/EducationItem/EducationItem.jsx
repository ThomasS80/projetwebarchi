/* eslint-disable react/prop-types */
import * as React from 'react'
import { Grid, Menu, MenuItem, Paper, IconButton, Tooltip } from '@mui/material'
import MoreVertIcon from '@mui/icons-material/MoreVert'
import ManageAccountsIcon from '@mui/icons-material/ManageAccounts'
import styled from 'styled-components'

import { deleteEducation } from '../../store/actions/educations'
import { useDispatch } from 'react-redux'
import useAppContext from '../../contexts/AppContext'

const MenuButton = styled(IconButton)`
  position: absolute;
  right: 15px;
  top: 10px;  
`
const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
`

export default function EducationItem ({ education, handleEdit }) {
  const [anchorEl, setAnchorEl] = React.useState(null)
  const open = Boolean(anchorEl)
  const dispatch = useDispatch()
  const { showNotif, refreshData } = useAppContext()

  const isManager = education.module.manager === education.professor._id

  const handleClick = (event) => setAnchorEl(event.currentTarget)

  const handleClose = () => setAnchorEl(null)

  const editItemHandler = () => handleEdit(education)
  const deleteItemHandler = async () => {
    const deleted = await deleteEducation(education._id)(dispatch)
    if (!deleted.error) {
      showNotif({ type: 'success', message: 'La désinscription a bien été traité' })
      refreshData()
    } else {
      showNotif({ type: 'error', message: deleted.error })
    }
  }

  const getActions = () => {
    return [
      { label: 'Editer', handler: editItemHandler },
      { label: 'Désinscrire', handler: deleteItemHandler }
    ]
  }

  return (
    <Paper elevation={3} sx={{ width: '100%', padding: '12px' }}>
      <Grid container spacing={1}>
      <Grid item lg={4}>
        <b>Module:</b><span>{` ${education.module.title}`}</span><br/>
      </Grid>
      <Grid item lg={2}>
        <Wrapper>
          <b>Assuré par:</b>
          <span style={{ marginLeft: '8px' }}>{` ${education.professor.firstName} ${education.professor.lastName}`}</span>
          {isManager && <Tooltip title="Responsable module"><ManageAccountsIcon size="small" color="warning"/></Tooltip>}
        </Wrapper>
      </Grid>
      <Grid item lg={2}>
        <b>Groupes CM:</b><span>{` ${education.totalGroupsCM}`}</span><br/>
      </Grid>
      <Grid item lg={2}>
        <b>Groupes TD:</b><span>{` ${education.totalGroupsTD}`}</span><br/>
      </Grid>
      <Grid item lg={2}>
        <b>Groupes TP:</b><span>{` ${education.totalGroupsTP}`}</span><br/>
      </Grid>
    </Grid>

      <MenuButton
        aria-label="more"
        id="long-button"
        aria-controls={open ? 'long-menu' : undefined}
        aria-expanded={open ? 'true' : undefined}
        aria-haspopup="true"
        onClick={handleClick}
      >
        <MoreVertIcon />
      </MenuButton>
      <Menu
        id="long-menu"
        MenuListProps={{ 'aria-labelledby': 'long-button' }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
      >
        {getActions().map((option) => (<MenuItem key={option.label} onClick={option.handler}>{option.label} </MenuItem>))}
      </Menu>
    </Paper>
  )
}
