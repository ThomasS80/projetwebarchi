/* eslint-disable react/prop-types */
import * as React from 'react'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import DialogTitle from '@mui/material/DialogTitle'
import styled from 'styled-components'

const InoutsWrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-evenly;
    & * { margin-right: 3px; }
`

export default function EditModal ({ professor, setProfessor, open, handleClose, handleConfirm }) {
  const setUCHandler = (ev) => {
    const newUCValue = ev.target.value
    if (isNaN(newUCValue)) return

    setProfessor({ ...professor, minUC: parseInt(newUCValue) })
  }
  return (
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Modifier min. UC</DialogTitle>
        <DialogContent>
          <DialogContentText><b>Enseignant:</b> {professor.firstName + ' ' + professor.lastName}</DialogContentText>
          <InoutsWrapper>
          <TextField
            value={professor.minUC}
            onChange={setUCHandler}
            margin="dense"
            label="Groupes CM"
            variant="outlined"
            size="small"
          />
          </InoutsWrapper>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Annuler</Button>
          <Button onClick={handleConfirm}>Enregister</Button>
        </DialogActions>
      </Dialog>
  )
}
