import React from 'react'
import Fab from '@mui/material/Fab'
import AddIcon from '@mui/icons-material/Add'
import styled from 'styled-components'

const AddButton = styled(Fab)`
  position: absolute;
  top: 80px;
  right: 25px;
`

export default function FloatingAddButton (props) {
  return (
    <AddButton color="primary" aria-label="add" {...props}>
      <AddIcon />
    </AddButton>
  )
}
