/* eslint-disable react/prop-types */
import React from 'react'
import { Paper, Chip, IconButton, Menu, MenuItem } from '@mui/material'
import MoreVertIcon from '@mui/icons-material/MoreVert'
import styled from 'styled-components'
import Progress from '../Progress/Progress'
import useAppContext from '../../contexts/AppContext'
import { getTotalUC } from 'shared/utils'
import { deleteProfessor } from 'store/actions/professors'
import { useDispatch } from 'react-redux'

const Wrapper = styled(Paper)`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  padding 12px;
`

const getStatusColor = (status) => {
  switch (status) {
    case 'ATER': return 'error'
    case 'VACATAIRE': return 'warning'
    default: return 'info'
  }
}

export default function ProfessorItem ({ prof, handleEdit }) {
  const [anchorEl, setAnchorEl] = React.useState(null)
  const open = Boolean(anchorEl)

  const { ucRules, showNotif, refreshData } = useAppContext()
  const dispatch = useDispatch()

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  const editItemHandler = () => handleEdit(prof)

  const deleteItemHandler = async () => {
    const deleted = await deleteProfessor(prof._id)(dispatch)
    if (!deleted.error) {
      showNotif({ type: 'success', message: 'Le compte enseignant a été supprimé' })
      refreshData()
    } else {
      showNotif({ type: 'error', message: deleted.error })
    }
  }

  const getActions = () => {
    return [
      { label: 'Modifier', handler: editItemHandler },
      { label: 'Supprimer', handler: deleteItemHandler }
    ]
  }

  const totalGroups = prof.assignedCMGroups + prof.assignedTDGroups + prof.assignedTPGroups
  const totalUC = getTotalUC(prof, ucRules)

  return (
    <Wrapper elevation={3}>
        <div style={{ width: '15%' }}><h5>{prof.firstName} {prof.lastName}</h5></div>
        <div style={{ width: '15%' }}><b style={{ marginRight: '5px' }}>Status:</b><Chip label={prof.status} color={getStatusColor(prof.status)} size="small"/></div>
        <div style={{ width: '15%' }}><b style={{ marginRight: '5px' }}>Total UC/an:</b>{prof.minUC}</div>
        <div style={{ width: '15%' }}><b style={{ marginRight: '5px' }}>Groupes assurés:</b>{totalGroups}</div>
        <div style={{ width: '15%' }}><Progress value={(totalUC / prof.minUC) * 100} title={`${totalUC}/${prof.minUC} UC`} /></div>
        <div>
      <IconButton
        aria-label="more"
        id="long-button"
        aria-controls={open ? 'long-menu' : undefined}
        aria-expanded={open ? 'true' : undefined}
        aria-haspopup="true"
        onClick={handleClick}
      >
        <MoreVertIcon />
      </IconButton>
      <Menu
        id="long-menu"
        MenuListProps={{ 'aria-labelledby': 'long-button' }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
      >
        {getActions().map((option) => (
          <MenuItem key={option.label} onClick={option.handler}>
            {option.label}
          </MenuItem>
        ))}
      </Menu>
      </div>
    </Wrapper>
  )
}
