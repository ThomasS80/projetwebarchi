import * as React from 'react'
import PropTypes from 'prop-types'
import CircularProgress from '@mui/material/CircularProgress'
import Typography from '@mui/material/Typography'
import Box from '@mui/material/Box'

export default function Progress (props) {
  const progressValue = Math.min(props.value, 100)
  const color = progressValue < 50 ? 'error' : progressValue >= 90 ? 'success' : 'warning'
  return (
    <Box sx={{ position: 'relative', display: 'inline-flex' }} title={props.title}>
      <CircularProgress variant="determinate" color={color} {...props} value={progressValue} />
      <Box
        sx={{
          top: 0,
          left: 0,
          bottom: 0,
          right: 0,
          position: 'absolute',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center'
        }}
      >
        <Typography variant="caption" component="div" color="text.secondary">
          {`${Math.round(progressValue)}%`}
        </Typography>
      </Box>
    </Box>
  )
}

Progress.propTypes = {
  value: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired
}
