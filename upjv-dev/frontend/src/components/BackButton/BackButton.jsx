import React from 'react'
import Fab from '@mui/material/Fab'
import ArrowBackIcon from '@mui/icons-material/ArrowBack'
import styled from 'styled-components'
import { useNavigate } from 'react-router-dom'

const ReturnButton = styled(Fab)`
  position: absolute;
  top: 80px;
  left: 250px;
`

export default function BackButton (props) {
  const navigate = useNavigate()
  return (
    <ReturnButton color="default" aria-label="add" {...props} onClick={() => navigate(-1)}>
      <ArrowBackIcon />
    </ReturnButton>
  )
}
