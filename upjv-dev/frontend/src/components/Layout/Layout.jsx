import * as React from 'react'
import Box from '@mui/material/Box'
import Drawer from '@mui/material/Drawer'
import AppBar from '@mui/material/AppBar'
import Toolbar from '@mui/material/Toolbar'
import List from '@mui/material/List'
import Typography from '@mui/material/Typography'
import Divider from '@mui/material/Divider'
import ListItem from '@mui/material/ListItem'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import { useNavigate } from 'react-router-dom'
// import styled from 'styled-components'

import { node } from 'prop-types'
import { Link } from '@mui/material'
import { appRoutes } from '../../shared/appRoutes'
import useAuthContext from '../../contexts/AuthContext'
import logo from 'shared/images/logo.jpeg'

const drawerWidth = 240

const Layout = ({ children }) => {
  const [activeTab, setActiveTab] = React.useState(0)
  const navigate = useNavigate()
  const { user, handleLogout } = useAuthContext()

  const professorMenus = appRoutes.filter(r => !r.isAdmin)
  const routes = user.isAdmin ? appRoutes : professorMenus

  const navigateHandler = (path) => {
    const tab = appRoutes.find(r => r.path === path)
    if (path === 'logout') return handleLogout()

    if (tab) setActiveTab(tab.id)
    navigate('/' + path)
  }

  return (
        <Box sx={{ display: 'flex' }}>
            <AppBar position="fixed" sx={{ zIndex: (theme) => theme.zIndex.drawer + 1 }}>
                <Toolbar>
                    <Typography variant="h6" noWrap component="div">UPJV</Typography>
                </Toolbar>
            </AppBar>
            {user.token && <Drawer variant="permanent"
                sx={{
                  width: drawerWidth,
                  flexShrink: 0,
                  '& .MuiDrawer-paper': { width: drawerWidth, boxSizing: 'border-box', background: '#eeeeee' }
                }}>
                <Toolbar />
                <Box sx={{ overflow: 'auto', background: '#eeeeee' }}>
                    <List>
                        {routes.filter(r => r.group === 1).map(({ id, menuLabel, menuIcon, path }) => (
                            <ListItem button components={Link} key={path} onClick={() => navigateHandler(path)} selected={id === activeTab}>
                                <ListItemIcon>{menuIcon}</ListItemIcon>
                                <ListItemText primary={menuLabel} />
                            </ListItem>
                        ))}
                    </List>
                    <Divider />
                    <List>
                        {routes.filter(r => r.group === 2).map(({ id, menuLabel, menuIcon, path }) => (
                            <ListItem button components={Link} key={path} onClick={() => navigateHandler(path)} selected={id === activeTab}>
                                <ListItemIcon>{menuIcon}</ListItemIcon>
                                <ListItemText primary={menuLabel} />
                            </ListItem>
                        ))}
                    </List>
                </Box>
                <img src={logo} alt="UNJV-logo" height={150} width={150} style={{ marginTop: 'auto' }}/>
            </Drawer>}
            <Box component="main" sx={{ flexGrow: 1, p: 3, mt: '40px' }}>
                {children}
            </Box>
        </Box>
  )
}

Layout.propTypes = {
  children: node.isRequired
}

export default Layout
