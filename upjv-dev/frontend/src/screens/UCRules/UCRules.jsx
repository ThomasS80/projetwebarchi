import * as React from 'react'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import Paper from '@mui/material/Paper'
import EditIcon from '@mui/icons-material/Edit'
import { Divider, IconButton } from '@mui/material'
import useAppContext from '../../contexts/AppContext'
import EditModal from './EditModal'
import { useDispatch } from 'react-redux'
import { updateUCRules } from 'store/actions/ucRules'

export default function UCRules () {
  const [modalOpen, toggleOpen] = React.useState(false)
  const [rule, setRule] = React.useState(null)

  const dispatch = useDispatch()

  const { ucRules, showNotif, refreshData } = useAppContext()

  const openEditModalHandler = (_rule) => {
    setRule(_rule)
    toggleOpen(true)
  }

  const saveRuleHandler = async () => {
    const { CMFactor, TDFactor, TPFactor } = rule

    if (isNaN(CMFactor) || isNaN(TDFactor) || isNaN(TPFactor)) return

    const ruleCopy = { ...rule, CMFactor: +CMFactor, TDFactor: +TDFactor, TPFactor: +TPFactor }

    const saved = await updateUCRules(ruleCopy)(dispatch)
    if (!saved.error) {
      toggleOpen(false)
      setRule(null)
      showNotif({ type: 'success', message: 'La règle de calcul a été mis à jour' })
      refreshData()
    }
  }

  return (
    <TableContainer component={Paper} sx={{ marginTop: '40px' }}>
      {rule && <EditModal
        open={modalOpen}
        handleClose={() => toggleOpen(false)} h
        handleConfirm={saveRuleHandler}
        rule={rule}
        setRule={setRule}
      />}
      <h3>Régles de calcul des UC</h3>
      <Divider/>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Status</TableCell>
            <TableCell align="right">Coefficient  CM</TableCell>
            <TableCell align="right">Coefficient TP</TableCell>
            <TableCell align="right">Coefficient TD</TableCell>
            <TableCell align="right">Actions</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {ucRules.map((row) => (
            <TableRow
              key={row._id}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {row.status}
              </TableCell>
              <TableCell align="right">{row.CMFactor}</TableCell>
              <TableCell align="right">{row.TDFactor}</TableCell>
              <TableCell align="right">{row.TPFactor}</TableCell>
              <TableCell align="right"><IconButton size="small" onClick={() => openEditModalHandler(row)}><EditIcon/></IconButton></TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}
