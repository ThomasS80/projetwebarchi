/* eslint-disable react/prop-types */
import * as React from 'react'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import DialogTitle from '@mui/material/DialogTitle'
import styled from 'styled-components'

const InoutsWrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-evenly;
    & * { margin-right: 3px; }
`

export default function EditModal ({ rule, open, handleClose, handleConfirm, setRule }) {
  const setRuleHandler = (ev, type) => {
    if (isNaN(ev.target.value)) return

    const ruleCopy = { ...rule }
    ruleCopy[type] = ev.target.value
    setRule(ruleCopy)
  }
  return (
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Calcul des UC</DialogTitle>
        <DialogContent>
          <DialogContentText>Mettre à jour les régles de calcul des UC pour le status {rule.status}</DialogContentText>
          <InoutsWrapper>
          <TextField
            value={rule.CMFactor}
            onChange={(ev) => setRuleHandler(ev, 'CMFactor')}
            margin="dense"
            label="CM"
            variant="outlined"
            size="small"
          />
          <TextField
            value={rule.TDFactor}
            onChange={(ev) => setRuleHandler(ev, 'TDFactor')}
            margin="dense"
            label="TD"
            variant="outlined"
            size="small"
          />
          <TextField
            value={rule.TPFactor}
            onChange={(ev) => setRuleHandler(ev, 'TPFactor')}
            margin="dense"
            id="name"
            label="TP"
            variant="outlined"
            size="small"
          />
          </InoutsWrapper>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Annuler</Button>
          <Button onClick={handleConfirm}>Enregister</Button>
        </DialogActions>
      </Dialog>
  )
}
