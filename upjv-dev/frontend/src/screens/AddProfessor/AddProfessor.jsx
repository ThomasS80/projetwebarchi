import { Button, FormControl, FormHelperText, InputLabel, MenuItem, Paper, Select } from '@mui/material'
import React from 'react'
import styled, { css } from 'styled-components'
import { useForm, Controller } from 'react-hook-form'
import { useDispatch, useSelector } from 'react-redux'
import { yupResolver } from '@hookform/resolvers/yup'

import { professorSchema } from 'shared/validations'
import Input from '../../components/Input/Input'
import { profStatus } from '../../shared/consts'
import { addProfessor } from 'store/actions/professors'
import BackButton from '../../components/BackButton/BackButton'
import useAppContext from '../../contexts/AppContext'

const flexCenter = css`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;  
`

const Wrapper = styled.div`
  padding-top: 22px;
  flex-direction: column;
  ${flexCenter};
`

const FormWrapper = styled(Paper)`
  flex-direction: column;
  ${flexCenter};
`

const Form = styled.form`
  flex-direction: column;
  width: 500px;
  padding: 12px 16px;
  & * { margin-bottom: 5px; };
  ${flexCenter};
`

const FormError = styled.span`
  padding: 5px;
  margin: 12px 0px;
  background: red;
  color: white;
  width: 100%;
`
export default function AddProfessor () {
  const { showNotif } = useAppContext()
  const { error: addProfError } = useSelector(state => state.professors)
  const dispatch = useDispatch()
  const { register, handleSubmit, reset, control, formState: { errors } } = useForm({
    resolver: yupResolver(professorSchema),
    defaultValues: { status: '' }
  })

  const addProfHandler = async (formData) => {
    const added = await addProfessor(formData)(dispatch)
    console.log(added)
    if (!added.error) {
      reset()
      showNotif({ type: 'success', message: 'Le compte professeur a bien été créé' })
    }
  }

  return (
    <Wrapper>
      <BackButton/>
      <FormWrapper elevation={2}>
        <Form onSubmit={handleSubmit(addProfHandler)}>
          <h3>Ajouter un professeur</h3>
          {addProfError && <FormError>{addProfError}</FormError>}
          <Input register={register} field='firstName' error={errors.firstName} label="Prénom" />
          <Input register={register} field='lastName' error={errors.lastName} label="Nom" />
          <Input register={register} field='email' error={errors.email} label="Adresse mail" />
          <Controller
            control={control}
            name="status"
            render={({
              field: { onChange, value }
            }) => (
              <FormControl error={!!errors.status} fullWidth>
                <InputLabel id="status-select">Statut</InputLabel>
                <Select labelId="status-select" value={value} label="Statut" onChange={onChange} size='small'>
                  <MenuItem value="">Selectionner</MenuItem>
                  {profStatus.map(status => <MenuItem value={status} key={status}>{status}</MenuItem>)}
                </Select>
                <FormHelperText>{errors.status ? errors.status.message : ''}</FormHelperText>
              </FormControl>
            )}
          />
          <Input register={register} field='minUC' error={errors.minUC} label="Min. UC annuel" />
          <Button size="small" variant='contained' type="submit">Créer</Button>
        </Form>
      </FormWrapper>
    </Wrapper>
  )
}
