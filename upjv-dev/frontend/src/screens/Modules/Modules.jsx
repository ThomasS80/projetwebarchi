import React, { useState, useEffect } from 'react'
import { List, ListItem } from '@mui/material'
import ModuleItem from '../../components/ModuleItem/ModuleItem'
import Tabs from 'components/Tabs'
import useAppContext from '../../contexts/AppContext'
import { useNavigate } from 'react-router-dom'

export default function Modules () {
  const navigate = useNavigate()
  const { modules } = useAppContext()
  const [licenceModules, setLicenceModules] = useState([])
  const [masterModules, setMasterModules] = useState([])

  useEffect(() => {
    setLicenceModules(modules.filter(m => m.degree === 'Licence'))
    setMasterModules(modules.filter(m => m.degree !== 'Licence'))
  }, [modules])

  const tabs = [
    {
      label: `Licence (${licenceModules.length})`,
      content: <List>{licenceModules.map(module => <ListItem key={module._id} onClick={() => navigate(`/main/modules/${module._id}`)}>
        <ModuleItem module={module} />
        </ListItem>)}</List>
    },
    {
      label: `Master (${masterModules.length})`,
      content: <List>{masterModules.map(module => <ListItem key={module._id} onClick={() => navigate(`/main/modules/${module._id}`)}>
        <ModuleItem module={module} />
        </ListItem>)}</List>
    }
  ]
  return (
        <div>
          <Tabs tabs={tabs}/>
        </div>
  )
}
