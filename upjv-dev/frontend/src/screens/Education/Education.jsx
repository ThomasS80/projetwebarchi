import React, { useState } from 'react'
import AddButton from 'components/AddButton'
import { useNavigate } from 'react-router-dom'
import useAppContext from '../../contexts/AppContext'
import EducationItem from '../../components/EducationItem/EducationItem'
import { List, ListItem } from '@mui/material'
import EditModal from './EditModal'
import { useDispatch } from 'react-redux'
import { updateEducation } from 'store/actions/educations'

export default function Lectures () {
  const [education, setEducation] = useState(null)
  const [modalOpen, toggleModal] = useState(false)

  const navigate = useNavigate()
  const dispatch = useDispatch()
  const { educations, showNotif, refreshData } = useAppContext()

  const openEditModalHandler = (itemData) => {
    setEducation(itemData)
    toggleModal(true)
  }

  const saveEducationHandler = async () => {
    if (isNaN(education.totalGroupsCM) || isNaN(education.totalGroupsTD) || isNaN(education.totalGroupsTP)) return

    const saved = await updateEducation(education)(dispatch)

    if (!saved.error) {
      toggleModal(false)
      setEducation(null)
      showNotif({ type: 'success', message: 'Enseignement mis à jour' })
      refreshData()
    }
  }

  return (
    <div>
      {education && <EditModal education={education} setEducation={setEducation} open={modalOpen}
       handleClose={() => toggleModal(false)} handleConfirm={saveEducationHandler}/>}
      <AddButton onClick={() => navigate('/main/lectures/new')} />
      <h3>Liste des enseignements</h3>
      {!educations.length && <h4>La liste des enseignements est vide</h4>}
      <List>{educations.map(education => <ListItem key={education._id}><EducationItem education={education} handleEdit={openEditModalHandler} /></ListItem>)}</List>
    </div>
  )
}
