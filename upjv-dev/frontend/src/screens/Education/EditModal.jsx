/* eslint-disable react/prop-types */
import * as React from 'react'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import DialogTitle from '@mui/material/DialogTitle'
import styled from 'styled-components'

const InoutsWrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-evenly;
    & * { margin-right: 3px; }
`

export default function EditModal ({ education, open, handleClose, handleConfirm, setEducation }) {
  const setRuleHandler = (ev, type) => {
    if (isNaN(ev.target.value)) return

    const ruleCopy = { ...education }
    ruleCopy[type] = ev.target.value
    setEducation(ruleCopy)
  }
  return (
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Modifier enseignement</DialogTitle>
        <DialogContent>
          <DialogContentText><b>Module:</b> {education.module.title}</DialogContentText>
          <DialogContentText><b>Enseignant:</b> {education.professor.firstName + ' ' + education.professor.lastName}</DialogContentText>
          <InoutsWrapper>
          <TextField
            value={education.totalGroupsCM}
            onChange={(ev) => setRuleHandler(ev, 'totalGroupsCM')}
            margin="dense"
            label="Groupes CM"
            variant="outlined"
            size="small"
          />
          <TextField
            value={education.totalGroupsTD}
            onChange={(ev) => setRuleHandler(ev, 'totalGroupsTD')}
            margin="dense"
            label="Groupes TD"
            variant="outlined"
            size="small"
          />
          <TextField
            value={education.totalGroupsTP}
            onChange={(ev) => setRuleHandler(ev, 'totalGroupsTP')}
            margin="dense"
            id="name"
            label="Groupes TP"
            variant="outlined"
            size="small"
          />
          </InoutsWrapper>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Annuler</Button>
          <Button onClick={handleConfirm}>Enregister</Button>
        </DialogActions>
      </Dialog>
  )
}
