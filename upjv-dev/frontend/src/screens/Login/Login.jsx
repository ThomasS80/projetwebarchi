import { Button, Paper, TextField } from '@mui/material'
import React from 'react'
import styled, { css } from 'styled-components'
import { useForm } from 'react-hook-form'
import { useDispatch } from 'react-redux'
import { yupResolver } from '@hookform/resolvers/yup'

import logo from 'shared/images/logo.jpeg'
import { loginUser } from '../../store/actions/auth'
import { loginSchema } from 'shared/validations'
import useAuthContext from '../../contexts/AuthContext'

const flexCenter = css`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;  
`

const Wrapper = styled.div`
  padding-top: 22px;
  flex-direction: column;
  ${flexCenter};
`

const FormWrapper = styled(Paper)`
  flex-direction: column;
  ${flexCenter};
`

const Form = styled.form`
  flex-direction: column;
  width: 500px;
  padding: 12px 16px;
  & * { margin-bottom: 5px; };
  ${flexCenter};
`

const FormError = styled.span`
  padding: 5px;
  margin: 12px 0px;
  background: red;
  color: white;
  width: 100%;
`

export default function Login () {
  const { authError } = useAuthContext()
  const dispatch = useDispatch()
  const { register, handleSubmit, formState: { errors } } = useForm({
    resolver: yupResolver(loginSchema)
  })

  const loginHandler = async (formData) => {
    await loginUser(formData)(dispatch)
  }

  return (
    <Wrapper>
      <FormWrapper elevation={2}>
        <img src={logo} alt='UJPV-logo' height={150} width={150}/>
        <Form onSubmit={handleSubmit(loginHandler)}>
          <h3>Connecter vous à votre espace</h3>
          {authError && <FormError>{authError}</FormError>}
          <TextField {...register('email')} label="Adresse mail" placeholder='john.doe@email.com'
           error={!!errors.email} helperText={errors.email ? errors.email.message : ''} size='small' fullWidth/>
          <TextField {...register('password')} label="Mot de passe" type="password"
          error={!!errors.password} helperText={errors.password ? errors.password.message : ''} size='small' fullWidth/>
          <Button size="small" variant='contained' type="submit">Connexion</Button>
        </Form>
      </FormWrapper>
    </Wrapper>
  )
}
