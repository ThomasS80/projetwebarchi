import { Button, Paper } from '@mui/material'
import React from 'react'
import styled, { css } from 'styled-components'
import { useForm } from 'react-hook-form'
import { useDispatch } from 'react-redux'
import { yupResolver } from '@hookform/resolvers/yup'

import { changeUserPassword } from '../../store/actions/auth'
import { changePasswordSchema } from 'shared/validations'
import useAuthContext from '../../contexts/AuthContext'
import Input from '../../components/Input/Input'

const flexCenter = css`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;  
`

const Wrapper = styled.div`
  padding-top: 22px;
  flex-direction: column;
  ${flexCenter};
`

const FormWrapper = styled(Paper)`
  flex-direction: column;
  ${flexCenter};
`

const Form = styled.form`
  flex-direction: column;
  width: 500px;
  padding: 12px 16px;
  & * { margin-bottom: 5px; };
  ${flexCenter};
`

const FormError = styled.span`
  padding: 5px;
  margin: 12px 0px;
  background: red;
  color: white;
  width: 100%;
`

export default function MyAccount () {
  const dispatch = useDispatch()
  const { user, authError, showNotif } = useAuthContext()
  const { register, handleSubmit, reset, formState: { errors } } = useForm({
    resolver: yupResolver(changePasswordSchema),
    defaultValues: { email: user.email }
  })

  const changePasswordHandler = async (formData) => {
    const changed = await changeUserPassword(formData)(dispatch)
    if (!changed.error) {
      reset()
      showNotif({ type: 'success', message: 'Votre mot de passe a été mis à jour' })
    }
  }

  return (
    <Wrapper>
      <FormWrapper elevation={2}>
        <Form onSubmit={handleSubmit(changePasswordHandler)}>
          <h3>Changer votre mot de passe</h3>
          {authError && <FormError>{authError}</FormError>}
          <Input register={register} field='email' label="Adresse mail" disabled/>
          <Input register={register} field='currentPassword' error={errors.currentPassword} label="Mot de passe actuel" type='password'/>
          <Input register={register} field='newPassword' error={errors.newPassword} label="Nouveau mot de passe" type='password'/>
          <Input register={register} field='passwordConfirmation' error={errors.passwordConfirmation} label="Confirmer mot de passe" type='password'/>
          <Button size="small" variant='contained' type="submit">Changer</Button>
        </Form>
      </FormWrapper>
    </Wrapper>
  )
}
