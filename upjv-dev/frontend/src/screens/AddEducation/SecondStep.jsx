/* eslint-disable react/prop-types */
import React from 'react'
import { FormControl, Grid, InputLabel, MenuItem, Select } from '@mui/material'

const createOptionsArray = ({ CM, TD, TP }) => {
  const leftCMGroups = CM.totalGroups - CM.assignedGroups

  let CMRange = [0]
  const TDRange = [...Array(TD.totalGroups - TD.assignedGroups + 1).keys()]
  const TPRange = [...Array(TP.totalGroups - TP.assignedGroups + 1).keys()]

  if (leftCMGroups > 1) {
    CMRange = [...Array(CM.totalGroups - CM.assignedGroups + 1).keys()]
  } else if (leftCMGroups === 1) {
    CMRange = [0, 0.5, 1]
  } else if (leftCMGroups === 0.5) {
    CMRange = [0, 0.5]
  }

  return { CMRange, TDRange, TPRange }
}

export default function SecondStep ({ moduleInfo, groups, setGroups }) {
  const { CM, TD, TP } = moduleInfo
  const { CMRange, TDRange, TPRange } = createOptionsArray({ CM, TD, TP })

  const CMOptions = CMRange.length ? CMRange : [0]
  const TDOptions = TDRange.length ? TDRange : [0]
  const TPOptions = TPRange.length ? TPRange : [0]

  const setGroupsHandler = (ev, type) => {
    const groupsCopy = { ...groups }
    groupsCopy[type] = ev.target.value
    setGroups(groupsCopy)
  }

  return (
    <div>
        <form>
            <Grid container spacing={2}>
                <Grid lg={3} item>
                    <h3 style={{ margin: 0 }}>Nombre de groupes</h3>
                </Grid>
                <Grid lg={3} item>
                    <FormControl fullWidth>
                        <InputLabel id="status-select">Groupes CM</InputLabel>
                        <Select labelId="status-select" value={groups.CM} label="Groupes CM" onChange={(ev) => setGroupsHandler(ev, 'CM')} size='small'>
                        <MenuItem value="">Selectionner</MenuItem>
                        {CMOptions.map(count => <MenuItem value={count} key={count}>{count}</MenuItem>)}
                        </Select>
                    </FormControl>
                </Grid>
                <Grid lg={3} item>
                    <FormControl fullWidth>
                        <InputLabel id="status-select">Groupes TD</InputLabel>
                        <Select labelId="status-select" value={groups.TD} label="Groupes TD" onChange={(ev) => setGroupsHandler(ev, 'TD')} size='small'>
                        <MenuItem value="">Selectionner</MenuItem>
                        {TDOptions.map(count => <MenuItem value={count} key={count}>{count}</MenuItem>)}
                        </Select>
                    </FormControl>
                </Grid>
                <Grid lg={3} item>
                    <FormControl fullWidth>
                        <InputLabel id="status-select">Groupes TP</InputLabel>
                        <Select labelId="status-select" value={groups.TP} label="Groupes TP" onChange={(ev) => setGroupsHandler(ev, 'TP')} size='small'>
                        <MenuItem value="">Selectionner</MenuItem>
                        {TPOptions.map(count => <MenuItem value={count} key={count}>{count}</MenuItem>)}
                        </Select>
                    </FormControl>
                </Grid>
            </Grid>
        </form>
    </div>
  )
}
