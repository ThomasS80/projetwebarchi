import * as React from 'react'
import Box from '@mui/material/Box'
import Stepper from '@mui/material/Stepper'
import Step from '@mui/material/Step'
import StepLabel from '@mui/material/StepLabel'
import Button from '@mui/material/Button'

import useAppContext from 'contexts/AppContext'
import FirstStep from './FirstStep'
import SecondStep from './SecondStep'
import ThirdStep from './ThirdStep'
import DoneStep from './DoneStep'
import ModuleItem from '../../components/ModuleItem/ModuleItem'
import styled from 'styled-components'
import BackButton from '../../components/BackButton/BackButton'
import { addEducation } from 'store/actions/educations'
import { useDispatch } from 'react-redux'

const steps = ['Choisir le module', 'Préciser les groupes', 'Finaliser Inscription']
const initialGroups = { CM: 0, TD: 0, TP: 0 }

const StepError = styled.span`
  background: red;
  color: white;
  padding: 5px;
  font-weight: bold;
  font-size: 1.2rem;
  width: 100%;
  border-radius: 5px;;
`

export default function AddEducationStepper () {
  const [activeStep, setActiveStep] = React.useState(0)
  const [skipped, setSkipped] = React.useState(new Set())
  const [module, setModule] = React.useState(null)
  const [professor, setProfessor] = React.useState(null)
  const [groups, setGroups] = React.useState(initialGroups)
  const [stepError, setStepError] = React.useState('')

  const { modules, professors, currentProfessor, refreshData } = useAppContext()
  const dispatch = useDispatch()

  const isStepSkipped = (step) => {
    return skipped.has(step)
  }

  const isStepValid = () => {
    setStepError('')

    switch (activeStep) {
      case 0: {
        if (!module || !professor) {
          setStepError('Veuillez choisir un enseignant et un module')
          return false
        }
        return true
      }
      case 1: {
        if (groups.CM) {
          const hasTD = module.TD.totalGroups - module.TD.assignedGroups !== 0
          const hasTP = module.TP.totalGroups - module.TP.assignedGroups !== 0
          if (!groups.TD && hasTD) {
            setStepError('Veuillez choisir au moins un groupe de TD')
            return false
          }
          if (!groups.TP && hasTP) {
            setStepError('Veuillez choisir au moins un groupe de TP')
            return false
          }
          return true
        }
        if (groups.CM === 0 && groups.TD === 0 && groups.TP === 0) {
          setStepError('Veuillez choisir au moins un groupe')
          return false
        }
        return true
      }
      default: return true
    }
  }

  const handleNext = () => {
    let newSkipped = skipped

    if (!isStepValid()) return

    if (activeStep === 0) setGroups(initialGroups)

    if (isStepSkipped(activeStep)) {
      newSkipped = new Set(newSkipped.values())
      newSkipped.delete(activeStep)
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1)
    setSkipped(newSkipped)
  }

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1)
  }

  const handleReset = () => {
    setModule(null)
    setProfessor(null)
    setGroups(initialGroups)
    setActiveStep(0)
  }

  const handleSubmit = async () => {
    const eduInfo = {
      totalGroupsCM: groups.CM,
      totalGroupsTD: groups.TD,
      totalGroupsTP: groups.TP,
      moduleId: module._id,
      professorId: professor._id
    }
    const created = await addEducation(eduInfo)(dispatch)
    if (created.error) return setStepError(created.error)
    refreshData()
    handleNext()
  }

  const getCurrentStepView = () => {
    switch (activeStep) {
      case 0: return <FirstStep
      professor={professor} professors={currentProfessor ? [currentProfessor] : professors}
      setProfessor={setProfessor} modules={modules} module={module} setModule={setModule} isAdmin/>
      case 1: return <SecondStep moduleInfo={module} groups={groups} setGroups={setGroups} />
      case 2: return <ThirdStep groups={groups} />
      case 3: return <DoneStep />
      default:
        break
    }
  }

  return (
    <Box sx={{ width: '100%', marginTop: '50px' }}>
      <BackButton/>
      <h3 style={{ marginLeft: '35px' }}>Ajouter un enseignement</h3>
      <Stepper activeStep={activeStep} sx={{ width: '60%', margin: '0px auto' }}>
        {steps.map((label, index) => {
          const stepProps = {}
          const labelProps = {}

          if (isStepSkipped(index)) {
            stepProps.completed = false
          }
          return (
            <Step key={label} {...stepProps} sx={{ color: 'red' }}>
              <StepLabel {...labelProps}>{label}</StepLabel>
            </Step>
          )
        })}
      </Stepper>
      <Box sx={{ width: '60%', margin: '35px auto' }}>
        {stepError && <StepError>{stepError}</StepError>}
        {module && activeStep !== steps.length &&
          <Box><h5>Module choisi</h5><ModuleItem module={module} minimal="true"/></Box>
        }
        <Box sx={{ marginTop: '22px' }}>{getCurrentStepView()}</Box>
        <Box sx={{ display: 'flex', flexDirection: 'row', pt: 2 }}>
            {activeStep !== steps.length &&
            <>
              <Button color="inherit" disabled={activeStep === 0} onClick={handleBack} sx={{ mr: 1 }} >
                Précédent
              </Button>
              <Box sx={{ flex: '1 1 auto' }} />
              <Button
              onClick={activeStep === steps.length - 1 ? handleSubmit : handleNext}
              variant={activeStep === steps.length - 1 ? 'contained' : 'text'}>
                {activeStep === steps.length - 1 ? 'Finaliser Inscription' : 'Suivant'}
              </Button>
            </>
            }
            {activeStep === steps.length && <Button onClick={handleReset}>Terminer</Button>}
          </Box>
      </Box>
    </Box>
  )
}
