/* eslint-disable react/prop-types */
import React from 'react'
import { Autocomplete, TextField } from '@mui/material'

export default function FirstStep ({ modules, module, setModule, professor, professors, setProfessor, isAdmin }) {
  return (
    <div>
        {isAdmin &&
        <Autocomplete
          value={professor}
          onChange={(_, newValue) => setProfessor(newValue)}
          options={professors}
          getOptionLabel={(option) => `${option.firstName} ${option.lastName}`}
          fullWidth
          sx={{ marginBottom: '25px' }}
          renderInput={(params) => <TextField {...params} size="small" label="Enseignant" />}
        />}
        <Autocomplete
          value={module}
          onChange={(_, newValue) => setModule(newValue)}
          options={modules.sort((a, b) => -b.degree.localeCompare(a.degree))}
          groupBy={(option) => option.degree}
          getOptionLabel={(option) => `${option.title} (${option.ref})`}
          fullWidth
          renderInput={(params) => <TextField {...params} size="small" label="Module" />}
        />
    </div>
  )
}
