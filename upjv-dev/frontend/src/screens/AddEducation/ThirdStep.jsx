/* eslint-disable react/prop-types */
import React from 'react'
import { Box, Paper } from '@mui/material'
import styled from 'styled-components'

const Wrapper = styled(Paper)`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  padding 12px;
`
export default function ThirdStep ({ groups }) {
  return (
      <Box>
        <h5>Groupes choisis</h5>
        <Wrapper elevation={3}>
            <div style={{ width: '33%' }}><b style={{ marginRight: '5px' }}>Groupes CM à assurer:</b>{groups.CM}</div>
            <div style={{ width: '33%' }}><b style={{ marginRight: '5px' }}>Groupes TD à assurer:</b>{groups.TD}</div>
            <div style={{ width: '33%' }}><b style={{ marginRight: '5px' }}>Groupes TP à assurer:</b>{groups.TP}</div>
        </Wrapper>
      </Box>
  )
}
