import React, { useEffect, useState } from 'react'
import { useLocation } from 'react-router-dom'
import { fetchModuleEducations } from 'store/actions/educations'
import { useDispatch } from 'react-redux'
import { List, ListItem } from '@mui/material'

import useAppContext from 'contexts/AppContext'
import ModuleItem from 'components/ModuleItem/ModuleItem'
import EducationItem from 'components/EducationItem/EducationItem'

export default function ModuleDetails () {
  const [educations, setEducations] = useState([])
  const [module, setModule] = useState(null)
  const dispatch = useDispatch()
  const { modules } = useAppContext()

  const { pathname } = useLocation()
  const splitPath = pathname.split('/')
  const moduleId = splitPath[splitPath.length - 1]

  const getModuleEducations = async (id) => {
    const { data } = await fetchModuleEducations(id)(dispatch)
    setEducations(data)
  }

  useEffect(() => {
    if (moduleId) {
      const mod = modules.find(m => m._id === moduleId)
      setModule(mod)
      getModuleEducations(moduleId)
    }
  }, [])

  return (
    <div style={{ marginTop: '25px' }}>
      {module && <ModuleItem module={module} />}
      <h3>Liste des enseignements</h3>
      {!educations.length && <h5>Aucun enseignement trouvé pour ce module</h5>}
      <List>{educations.map(education => <ListItem key={education._id}><EducationItem education={education} /></ListItem>)}</List>
    </div>
  )
}
