import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import styled from 'styled-components'
import { ListItem, List } from '@mui/material'
import useAppContext from 'contexts/AppContext'
import ProfessorItem from 'components/ProfessorItem'
import AddButton from 'components/AddButton'
import EditProfessor from 'components/EditProfessor'
import { updateProfessor } from '../../store/actions/professors'
import { useDispatch } from 'react-redux'

const Wrapper = styled.div`
`

export default function Professors () {
  const [professor, setProfessor] = useState(null)
  const [modalOpen, toggleModal] = useState(false)

  const navigate = useNavigate()
  const dispatch = useDispatch()
  const { professors, showNotif, refreshData } = useAppContext()

  const openEditModalHandler = (itemData) => {
    setProfessor(itemData)
    toggleModal(true)
  }

  const saveProfessorHandler = async () => {
    if (isNaN(professor.minUC)) return

    const saved = await updateProfessor(professor)(dispatch)
    if (!saved.error) {
      toggleModal(false)
      setProfessor(null)
      showNotif({ type: 'success', message: 'Les UC sont mises à jour avec succès' })
      refreshData()
    }
  }

  return (
    <Wrapper>
      {professor && <EditProfessor professor={professor} setProfessor={setProfessor} open={modalOpen}
       handleClose={() => toggleModal(false)} handleConfirm={saveProfessorHandler} />}
      <AddButton onClick={() => navigate('/main/professors/new')} />
      <h3>Liste des enseignants</h3>
      {!professors.length && <h4>Aucun enseignant trouvé</h4>}
      <List>{professors.map(prof => <ListItem key={prof._id}><ProfessorItem prof={prof} handleEdit={openEditModalHandler} /></ListItem>)}</List>
    </Wrapper>
  )
}
