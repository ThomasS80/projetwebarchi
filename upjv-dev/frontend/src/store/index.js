import {
  createStore, applyMiddleware, combineReducers, compose
} from 'redux'
import thunk from 'redux-thunk'

import { authReducer, modulesReducer, professorsReducer, ucRulesReducer, educationsReducer } from './reducers'

// root reducer
const rootReducer = combineReducers({
  auth: authReducer,
  modules: modulesReducer,
  professors: professorsReducer,
  ucRules: ucRulesReducer,
  educations: educationsReducer
})

// use redux dev tools
const composeEnhancers = typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
  ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
  : compose

const enhancer = composeEnhancers(applyMiddleware(thunk))

// create redux store
const store = createStore(rootReducer, enhancer)

export default store
