import axios from 'shared/axios'
import * as actions from '../reducers/ucRules'
import { getHeaders } from '../../shared/axios'

const endpoint = '/uc-rules'

export function fetchUCRules () {
  return async function fetchAsync (dispatch) {
    dispatch(actions.fetchUCRulesStart())
    try {
      const response = await axios.get(endpoint, getHeaders())

      if (response.data.error) throw new Error(response.data.error.message)

      dispatch(actions.fetchUCRulesSuccess(response.data.data))
    } catch (error) {
      dispatch(actions.fetchUCRulesError(error.message))
    }
  }
}

export function updateUCRules ({ _id, ...payload }) {
  return async function fetchAsync (dispatch) {
    try {
      const response = await axios.patch(`${endpoint}/${_id}`, payload, getHeaders())

      if (response.data.error) throw Error(response.data.error.message)

      return response.data
    } catch (error) {
      dispatch(actions.fetchUCRulesError(error.message))
      return { error: error.message }
    }
  }
}
