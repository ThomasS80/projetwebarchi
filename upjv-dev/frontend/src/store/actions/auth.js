import axios from 'shared/axios'
import * as actions from '../reducers/auth'
import { getHeaders } from '../../shared/axios'

const loginAndpoint = '/users/login'
const changePasswordAndpoint = '/users/change-password'

export function loginUser (payload) {
  return async function loginUserAsync (dispatch) {
    dispatch(actions.loginUserStart())
    try {
      const response = await axios.post(loginAndpoint, payload)

      if (response.data.error) throw new Error(response.data.error.message)

      localStorage.setItem('local-user', JSON.stringify(response.data))
      dispatch(actions.loginUserSuccess(response.data))
    } catch (error) {
      dispatch(actions.loginUserError(error.message))
    }
  }
}

export function changeUserPassword (payload) {
  return async function loginUserAsync (dispatch) {
    dispatch(actions.loginUserStart())
    try {
      const response = await axios.post(changePasswordAndpoint, payload, getHeaders())

      if (response.data.error) throw new Error(response.data.error.message)

      return response.data
    } catch (error) {
      dispatch(actions.loginUserError(error.message))
      return { error: error.message }
    }
  }
}
