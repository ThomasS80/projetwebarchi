import axios from 'shared/axios'
import * as actions from '../reducers/modules'
import { getHeaders } from '../../shared/axios'

const endpoint = '/modules'

export function fetchModules () {
  return async function fetchModulesAsync (dispatch) {
    dispatch(actions.fetchModulesStart())
    try {
      const response = await axios.get(endpoint, getHeaders())

      if (response.data.error) throw new Error(response.data.error.message)

      dispatch(actions.fetchModulesSuccess(response.data.data))
    } catch (error) {
      dispatch(actions.fetchModulesError(error.message))
    }
  }
}
