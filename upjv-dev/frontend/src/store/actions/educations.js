import axios from 'shared/axios'
import * as actions from '../reducers/educations'
import { getHeaders } from '../../shared/axios'

const endpoint = '/educations'
const moduleEducationsEndpoint = '/educations/module'

export function fetchEducations () {
  return async function fetchAsync (dispatch) {
    dispatch(actions.fetchEducationsStart())
    try {
      const response = await axios.get(endpoint, getHeaders())

      if (response.data.error) throw new Error(response.data.error)

      dispatch(actions.fetchEducationsSuccess(response.data.data))
    } catch (error) {
      dispatch(actions.fetchEducationsError(error.message))
    }
  }
}

export function fetchModuleEducations (moduleId) {
  return async function fetchAsync (dispatch) {
    try {
      const response = await axios.get(moduleEducationsEndpoint + '/' + moduleId, getHeaders())

      if (response.data.error) throw new Error(response.data.error)
      return response.data
    } catch (error) {
      dispatch(actions.fetchEducationsError(error.message))
      return { error: { message: error.message } }
    }
  }
}

export function addEducation (payload) {
  return async function fetchAsync (dispatch) {
    dispatch(actions.addEducationStart())
    try {
      const response = await axios.post(endpoint, payload, getHeaders())

      if (response.data.error) throw Error(response.data.error.message)

      dispatch(actions.addEducationSuccess(response.data.data))
      return response.data
    } catch (error) {
      dispatch(actions.addEducationError(error.message))
      return { error: error.message }
    }
  }
}

export function updateEducation (educationInfo) {
  return async function fetchAsync (dispatch) {
    const { _id } = educationInfo
    try {
      const response = await axios.patch(`${endpoint}/${_id}`, educationInfo, getHeaders())

      if (response.data.error) throw Error(response.data.error.message)

      return response.data
    } catch (error) {
      dispatch(actions.addEducationError(error.message))
      return { error: error.message }
    }
  }
}

export function deleteEducation (id) {
  return async function fetchAsync (dispatch) {
    try {
      const response = await axios.delete(`${endpoint}/${id}`, getHeaders())

      if (response.data.error) throw Error(response.data.error.message)

      return response.data
    } catch (error) {
      dispatch(actions.addEducationError(error.message))
      return { error: error.message }
    }
  }
}
