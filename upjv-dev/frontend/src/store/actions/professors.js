import axios from 'shared/axios'
import * as actions from '../reducers/professors'
import { getHeaders } from '../../shared/axios'

const endpoint = '/professors'

export function fetchProfessors () {
  return async function fetchProfessorsAsync (dispatch) {
    dispatch(actions.fetchProfessorsStart())
    try {
      const response = await axios.get(endpoint, getHeaders())

      if (response.data.error) throw new Error(response.data.error)

      dispatch(actions.fetchProfessorsSuccess(response.data.data))
    } catch (error) {
      dispatch(actions.fetchProfessorsError(error.message))
    }
  }
}

export function addProfessor (payload) {
  return async function fetchProfessorsAsync (dispatch) {
    dispatch(actions.addProfessorStart())
    try {
      const response = await axios.post(endpoint, payload, getHeaders())

      if (response.data.error) throw Error(response.data.error.message)

      dispatch(actions.addProfessorSuccess(response.data.data))
      return response.data
    } catch (error) {
      dispatch(actions.addProfessorError(error.message))
      return { error: error.message }
    }
  }
}

export function updateProfessor (profInfo) {
  const { _id } = profInfo
  return async function updateAsync (dispatch) {
    try {
      const response = await axios.patch(`${endpoint}/${_id}`, profInfo, getHeaders())
      if (response.data.error) throw Error(response.data.error.message)

      return response.data
    } catch (error) {
      dispatch(actions.addProfessorError(error.message))
      return { error: error.message }
    }
  }
}

export function deleteProfessor (profId) {
  return async function fetchProfessorsAsync (dispatch) {
    try {
      const response = await axios.delete(`${endpoint}/${profId}`, getHeaders())
      if (response.data.error) throw Error(response.data.error.message)

      return response.data
    } catch (error) {
      dispatch(actions.addProfessorError(error.message))
      return { error: error.message }
    }
  }
}
