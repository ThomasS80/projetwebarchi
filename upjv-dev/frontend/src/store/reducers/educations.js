import * as actionTypes from '../actionTypes'

const DEFAULT_STATE = {
  data: [],
  error: null,
  fetching: false,
  adding: false
}

// action creators
// ________________
export function fetchEducationsStart () {
  return { type: actionTypes.FETCH_EDUCATIONS_START }
}

export function fetchEducationsSuccess (educations) {
  return { type: actionTypes.FETCH_EDUCATIONS_SUCCEED, payload: { educations } }
}

export function fetchEducationsError (error) {
  return { type: actionTypes.FETCH_EDUCATIONS_FAIL, payload: { error } }
}

export function addEducationStart () {
  return { type: actionTypes.ADD_EDUCATION_START }
}
export function addEducationSuccess (education) {
  return { type: actionTypes.ADD_EDUCATION_SUCCEED, payload: { education } }
}
export function addEducationError (error) {
  return { type: actionTypes.ADD_EDUCATION_FAIL, payload: { error } }
}

const reducer = (state = DEFAULT_STATE, action) => {
  const { type } = action

  switch (type) {
    case actionTypes.ADD_EDUCATION_START: {
      return { ...state, adding: true }
    }

    case actionTypes.ADD_EDUCATION_SUCCEED: {
      return {
        ...state,
        adding: false,
        error: null,
        data: [action.payload.education, ...state.data]
      }
    }

    case actionTypes.ADD_EDUCATION_FAIL: {
      return { ...state, adding: false, error: action.payload.error }
    }

    case actionTypes.FETCH_EDUCATIONS_START: {
      return { ...state, fetching: true }
    }

    case actionTypes.FETCH_EDUCATIONS_SUCCEED: {
      return {
        ...state,
        fetching: false,
        error: null,
        data: action.payload.educations
      }
    }

    case actionTypes.FETCH_EDUCATIONS_FAIL: {
      return { ...state, fetching: false, error: action.payload.error }
    }

    case actionTypes.CLEAR_STORE: {
      return DEFAULT_STATE
    }

    default:
      return state
  }
}

export default reducer
