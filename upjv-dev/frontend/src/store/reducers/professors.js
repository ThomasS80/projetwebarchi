import * as actionTypes from '../actionTypes'

const DEFAULT_STATE = {
  data: [],
  error: null,
  fetching: false,
  adding: false
}

// action creators
// ________________
export function fetchProfessorsStart () {
  return { type: actionTypes.FETCH_PROFESSORS_START }
}

export function fetchProfessorsSuccess (professors) {
  return { type: actionTypes.FETCH_PROFESSORS_SUCCEED, payload: { professors } }
}

export function fetchProfessorsError (error) {
  return { type: actionTypes.FETCH_PROFESSORS_FAIL, payload: { error } }
}

export function addProfessorStart () {
  return { type: actionTypes.ADD_PROFESSOR_START }
}
export function addProfessorSuccess (newProf) {
  return { type: actionTypes.ADD_PROFESSOR_SUCCEED, payload: { newProf } }
}
export function addProfessorError (error) {
  return { type: actionTypes.ADD_PROFESSOR_FAIL, payload: { error } }
}

const reducer = (state = DEFAULT_STATE, action) => {
  const { type } = action

  switch (type) {
    case actionTypes.ADD_PROFESSOR_START: {
      return { ...state, adding: true }
    }

    case actionTypes.ADD_PROFESSOR_SUCCEED: {
      return {
        ...state,
        adding: false,
        error: null,
        data: [action.payload.newProf, ...state.data]
      }
    }

    case actionTypes.ADD_PROFESSOR_FAIL: {
      return { ...state, adding: false, error: action.payload.error }
    }

    case actionTypes.FETCH_PROFESSORS_START: {
      return { ...state, fetching: true }
    }

    case actionTypes.FETCH_PROFESSORS_SUCCEED: {
      return {
        ...state,
        fetching: false,
        error: null,
        data: action.payload.professors
      }
    }

    case actionTypes.FETCH_PROFESSORS_FAIL: {
      return { ...state, fetching: false, error: action.payload.error }
    }

    case actionTypes.CLEAR_STORE: {
      return DEFAULT_STATE
    }

    default:
      return state
  }
}

export default reducer
