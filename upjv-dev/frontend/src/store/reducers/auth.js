import * as actionTypes from '../actionTypes'

const DEFAULT_STATE = {
  data: null,
  error: null,
  loading: false
}

// action creators
// ________________
export function loginUserStart () {
  return { type: actionTypes.LOGIN_USER_START }
}

export function loginUserSuccess (user) {
  return { type: actionTypes.LOGIN_USER_SUCCEED, payload: { user } }
}

export function loginUserError (error) {
  return { type: actionTypes.LOGIN_USER_FAIL, payload: { error } }
}

export function clearStore () {
  return { type: actionTypes.CLEAR_STORE }
}

const reducer = (state = DEFAULT_STATE, action) => {
  const { type } = action

  switch (type) {
    case actionTypes.LOGIN_USER_START: {
      return { ...state, loading: true }
    }

    case actionTypes.LOGIN_USER_SUCCEED: {
      return {
        loading: false,
        error: null,
        data: action.payload.user
      }
    }

    case actionTypes.LOGIN_USER_FAIL: {
      return { ...state, loading: false, error: action.payload.error }
    }

    case actionTypes.CLEAR_STORE: {
      return DEFAULT_STATE
    }

    default:
      return state
  }
}

export default reducer
