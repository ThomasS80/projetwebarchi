import * as actionTypes from '../actionTypes'

const DEFAULT_STATE = {
  data: [],
  error: null,
  loading: false
}

// action creators
// ________________
export function fetchModulesStart () {
  return { type: actionTypes.FETCH_MODULES_START }
}

export function fetchModulesSuccess (modules) {
  return { type: actionTypes.FETCH_MODULES_SUCCEED, payload: { modules } }
}

export function fetchModulesError (error) {
  return { type: actionTypes.FETCH_MODULES_FAIL, payload: { error } }
}

const reducer = (state = DEFAULT_STATE, action) => {
  const { type } = action

  switch (type) {
    case actionTypes.FETCH_MODULES_START: {
      return { ...state, loading: true }
    }

    case actionTypes.FETCH_MODULES_SUCCEED: {
      return {
        loading: false,
        error: null,
        data: action.payload.modules
      }
    }

    case actionTypes.FETCH_MODULES_FAIL: {
      return { ...state, loading: false, error: action.payload.error }
    }

    case actionTypes.CLEAR_STORE: {
      return DEFAULT_STATE
    }

    default:
      return state
  }
}

export default reducer
