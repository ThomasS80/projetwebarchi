import * as yup from 'yup'
import { profStatus } from '../consts'

const schema = yup
  .object()
  .shape({
    firstName: yup.string().required('Champ obligatoire'),
    lastName: yup.string().required('Champ obligatoire'),
    email: yup.string().email().required('Champ obligatoire'),
    status: yup.string().oneOf(profStatus).required('Champ obligatoire'),
    minUC: yup.number().required().typeError('Renseigner un entier')
  })
  .required()

export default schema
