export { default as loginSchema } from './login'
export { default as changePasswordSchema } from './updatePassword'
export { default as professorSchema } from './professor'
