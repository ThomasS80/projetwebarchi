import * as yup from 'yup'

const schema = yup
  .object()
  .shape({
    currentPassword: yup.string().min(5).required(),
    newPassword: yup.string().min(5).required(),
    passwordConfirmation: yup.string().oneOf([yup.ref('newPassword'), null], 'Passwords must match')
  })
  .required()

export default schema
