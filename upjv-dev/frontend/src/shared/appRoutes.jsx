import React from 'react'
import ExitToAppIcon from '@mui/icons-material/ExitToApp'
import AccountBoxIcon from '@mui/icons-material/AccountBox'
import ViewModuleIcon from '@mui/icons-material/ViewModule'
import GroupsIcon from '@mui/icons-material/Groups'
import HistoryEduIcon from '@mui/icons-material/HistoryEdu'
import TuneIcon from '@mui/icons-material/Tune'

import Login from 'screens/Login'
import Modules from 'screens/Modules'
import Professors from 'screens/Professors'
import Lectures from 'screens/Education'
import MyAccount from 'screens/MyAccount'
import AddEducation from 'screens/AddEducation'
import AddProfessor from 'screens/AddProfessor/AddProfessor'
import UCRules from 'screens/UCRules'

export const appRoutes = [
  {
    id: 0,
    path: 'main/modules',
    element: <Modules/>,
    menuLabel: 'Modules',
    menuIcon: <ViewModuleIcon />,
    group: 1,
    isAdmin: false
  },
  {
    id: 1,
    path: 'main/lectures',
    element: <Lectures/>,
    menuLabel: 'Enseignements',
    menuIcon: <HistoryEduIcon />,
    group: 1,
    isAdmin: false
  },
  {
    id: 2,
    path: 'main/lectures/new',
    element: <AddEducation />,
    menuLabel: 'Ajouter Enseignement',
    menuIcon: null,
    group: 3,
    isAdmin: false
  },
  {
    id: 3,
    path: 'main/professors',
    element: <Professors />,
    menuLabel: 'Enseignants',
    menuIcon: <GroupsIcon />,
    group: 1,
    isAdmin: true
  },
  {
    id: 4,
    path: 'main/professors/new',
    element: <AddProfessor />,
    menuLabel: 'Ajouter Enseignant',
    menuIcon: null,
    group: 3,
    isAdmin: true
  },
  {
    id: 8,
    path: 'main/ucrules',
    element: <UCRules />,
    menuLabel: 'Calcul UC',
    menuIcon: <TuneIcon />,
    group: 1,
    isAdmin: true
  },
  {
    id: 5,
    path: 'main/account',
    element: <MyAccount />,
    menuLabel: 'Mon Compte',
    menuIcon: <AccountBoxIcon />,
    group: 2,
    isAdmin: false
  },
  {
    id: 6,
    path: 'logout',
    element: null,
    menuLabel: 'Déconnexion',
    menuIcon: <ExitToAppIcon />,
    group: 2,
    isAdmin: false
  },
  {
    id: 7,
    path: 'login',
    element: <Login/>,
    menuLabel: 'Login',
    menuIcon: null,
    group: 3,
    isAdmin: false
  }
]
