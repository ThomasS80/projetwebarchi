import axios from 'axios'

const instance = axios.create({ baseURL: process.env.REACT_APP_API_URL })

export const getHeaders = () => {
  const auth = JSON.parse(localStorage.getItem('local-user')) || {}
  return { headers: { Authorization: auth.token } }
}

export default instance
