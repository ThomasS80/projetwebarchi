export const getModuleAssignedGroupsRatio = (moduleInfo) => {
  const totalGroups = moduleInfo.CM.totalGroups + moduleInfo.TD.totalGroups + moduleInfo.TP.totalGroups
  const totalAssignedGroups = moduleInfo.CM.assignedGroups + moduleInfo.TD.assignedGroups + moduleInfo.TP.assignedGroups

  if (!totalGroups) return { ratio: 0, totalGroups, totalAssignedGroups }

  const ratio = totalAssignedGroups / totalGroups
  return { ratio, totalGroups, totalAssignedGroups }
}

export const getTotalUC = (prof, rules) => {
  const factors = rules.find(rule => rule.status === prof.status)
  const totalUC = prof.assignedCMGroups * factors.CMFactor + prof.assignedTDGroups * factors.TDFactor + prof.assignedTPGroups * factors.TPFactor

  return totalUC
}
