import { createTheme } from '@mui/material/styles'

const theme = createTheme({
  palette: {
    primary: {
      main: '#00345f'
    },
    secondary: {
      main: '#ee7c07'
    }
  }
})

export default theme
