import { ThemeProvider } from '@mui/material/styles'
import CssBaseline from '@mui/material/CssBaseline'
import React from 'react'
import { BrowserRouter, Routes, Route, Outlet } from 'react-router-dom'
import { Provider } from 'react-redux'

import store from 'store'
import Layout from './components/Layout'
import theme from './shared/theme'
import { appRoutes } from './shared/appRoutes'
import { AuthContextProvider } from './contexts/AuthContext'
import { AppContextProvider } from './contexts/AppContext'
import ModuleDetails from './screens/ModuleDetails/ModuleDetails'

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Provider store={store}>
        <BrowserRouter>
        <AuthContextProvider>
        <AppContextProvider>
          <Layout>
            <Routes>
              {appRoutes.map(({ id, path, element }) => {
                if (id === 0) {
                  return (<Route key={path} path={path} element={<Outlet />}>
                            <Route index element={element} />
                            <Route path=":module" element={<ModuleDetails />} />
                          </Route>
                  )
                }

                return <Route key={path} path={path} element={element} />
              })}
            </Routes>
          </Layout>
          </AppContextProvider>
          </AuthContextProvider>
        </BrowserRouter>
      </Provider>
    </ThemeProvider>
  )
}

export default App
