require("dotenv").config();
const cors = require("cors");
const mongoose = require("mongoose");
const express = require("express");
const path = require("path");

// import routes
const usersRoutes = require("./routes/users");
const modulesRoutes = require("./routes/modules");
const professorsRoutes = require("./routes/professors");
const ucRulesRoutes = require("./routes/ucRules");
const educationsRoutes = require("./routes/educations");

const PORT = process.env.PORT || 4000;

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, "public")));

// disable caching
app.disable("etag");

app.get("/api", (req, res) => res.json({ message: "API running" }));

// routes
app.use("/api/users", usersRoutes);
app.use("/api/modules", modulesRoutes);
app.use("/api/professors", professorsRoutes);
app.use("/api/uc-rules", ucRulesRoutes);
app.use("/api/educations", educationsRoutes);

// serve react built app
app.use("*", (req, res, next) => {
  if (/(.ico|.js|.css|.jpg|.png|.map)$/i.test(req.path)) {
    next();
  } else {
    res.header("Cache-Control", "private, no-cache, no-store, must-revalidate");
    res.header("Expires", "-1");
    res.header("Pragma", "no-cache");
    res.sendFile(path.join(__dirname, "public", "index.html"));
  }
});

// Connect to DB
mongoose.connect(process.env.DB_CONNECTION, () => {
  console.log("Connected to DB!");
});

// start listening to the server
app.listen(PORT, () => console.log("API running on port ", PORT));
