const Professor = require("../models/Professor");
const Education = require("../models/Education");
const Module = require("../models/Module");
const User = require("../models/User");

module.exports.findAll = async () => {
  const professors = await Professor.find();
  return { data: professors };
};

module.exports.findById = async (id) => {
  const professor = await Professor.findById(id);
  return { data: professor };
};

module.exports.findOne = async (userId) => {
  const professor = await Professor.findOne({ user: userId });
  return { data: [professor] };
};

module.exports.create = async (profInfo) => {
  const newProf = new Professor(profInfo);
  const savedProf = await newProf.save();

  return { data: savedProf };
};

module.exports.update = async (id, profInfo) => {
  const updatedProf = await Professor.findOneAndUpdate({ _id: id }, profInfo, {
    new: true,
  });
  return { data: updatedProf };
};

module.exports.delete = async (profId) => {
  // get all educations
  const educations = await Education.find({ professor: profId });

  // decrement groups to each module
  educations.forEach(async (education) => {
    await Module.findOneAndUpdate(
      { _id: education.module },
      {
        $inc: {
          "CM.assignedGroups": -education.totalGroupsCM,
          "TD.assignedGroups": -education.totalGroupsTD,
          "TP.assignedGroups": -education.totalGroupsTP,
        },
      }
    );
  });

  // delete professor assinged educations
  await Education.deleteMany({ professor: profId });

  // remove professor and user record
  const removed = await Professor.findByIdAndRemove(profId);
  await User.remove({ _id: removed.used });
  return { data: removed };
};
