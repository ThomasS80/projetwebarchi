const Module = require("../models/Module");

module.exports.findAll = async () => {
  const modules = await Module.find();
  return modules;
};

module.exports.findById = async (id) => {
  const module = await Module.findById(id);
  return module;
};
