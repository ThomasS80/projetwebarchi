const Education = require("../models/Education");
const Module = require("../models/Module");
const Professor = require("../models/Professor");

module.exports.findAll = async () => {
  const educations = await Education.find()
    .populate("module", "title  manager")
    .populate("professor", "firstName lastName");

  return { data: educations };
};

module.exports.findAllForProfessor = async (userId) => {
  const { id } = await Professor.findOne({ user: userId });
  const educations = await Education.find({ professor: id })
    .populate("module", "title  manager")
    .populate("professor", "firstName lastName");

  return { data: educations };
};

module.exports.findAllForModule = async (moduleId) => {
  const educations = await Education.find({ module: moduleId })
    .populate("module", "title manager")
    .populate("professor", "firstName lastName");

  return { data: educations };
};

module.exports.findById = async (id) => {
  const education = await Education.findById(id);
  return { data: education };
};

module.exports.create = async (payload) => {
  const { moduleId, professorId, ...eduInfo } = payload;
  const { totalGroupsCM, totalGroupsTD, totalGroupsTP } = eduInfo;

  const educationExists = await Education.findOne({
    professor: professorId,
    module: moduleId,
  });

  if (educationExists) {
    return {
      error: {
        message:
          "Un enseignement existe déjà, vous pouvez le modifier en ajoutant/supprimant des groupes",
      },
    };
  }

  // increment professor assigned groups
  await Professor.findByIdAndUpdate(
    { _id: professorId },
    {
      $inc: {
        assignedCMGroups: totalGroupsCM,
        assignedTDGroups: totalGroupsTD,
        assignedTPGroups: totalGroupsTP,
      },
    }
  );

  // asign module manager (only if one does not exist) && update assigned groups
  const module = await Module.findById(moduleId);
  await Module.findOneAndUpdate(
    { _id: moduleId },
    {
      $inc: {
        "CM.assignedGroups": totalGroupsCM,
        "TD.assignedGroups": totalGroupsTD,
        "TP.assignedGroups": totalGroupsTP,
      },
      $set: { manager: module.manager ? module.manager : professorId },
    }
  );

  // save new education document
  const newEdu = new Education({
    ...eduInfo,
    professor: professorId,
    module: moduleId,
  });
  const savedEdu = await newEdu.save();
  return { data: savedEdu };
};

module.exports.update = async (id, eduInfo) => {
  const updatedEdu = await Education.findOneAndUpdate({ _id: id }, eduInfo, {
    new: true,
  });
  return { data: updatedEdu };
};

module.exports.delete = async (id) => {
  const education = await Education.findById(id);
  const { module, professor, ...eduInfo } = education.toObject();
  const { totalGroupsCM, totalGroupsTD, totalGroupsTP } = eduInfo;

  // decrement professor assigned groups
  await Professor.findByIdAndUpdate(
    { _id: professor },
    {
      $inc: {
        assignedCMGroups: -totalGroupsCM,
        assignedTDGroups: -totalGroupsTD,
        assignedTPGroups: -totalGroupsTP,
      },
    }
  );

  // revoke module manager if professor is manager && update assigned groups
  const foundModule = await Module.findById(module);
  await Module.findOneAndUpdate(
    { _id: module },
    {
      $inc: {
        "CM.assignedGroups": -totalGroupsCM,
        "TD.assignedGroups": -totalGroupsTD,
        "TP.assignedGroups": -totalGroupsTP,
      },
      $set: {
        manager: foundModule.manager === professor ? "" : foundModule.manager,
      },
    }
  );
  const removed = await Education.findByIdAndRemove(id);
  return { data: removed };
};
