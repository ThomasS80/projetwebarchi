const UCRule = require("../models/UCRule");

module.exports.findAll = async () => {
  const UCRules = await UCRule.find();
  return { data: UCRules };
};

module.exports.findById = async (id) => {
  const UCRule = await UCRule.findById(id);
  return { data: UCRule };
};

module.exports.update = async (id, ruleInfo) => {
  const updatedRule = await UCRule.findOneAndUpdate({ _id: id }, ruleInfo, {
    new: true,
  });

  return { data: updatedRule };
};

module.exports.delete = async (id) => {
  const removed = await UCRule.findByIdAndRemove(id);
  return { data: removed };
};
