const EducationSummary = require("../models/EducationSummary");

module.exports.findAll = async () => {
  const summaries = await EducationSummary.find();
  return summaries;
};

module.exports.findById = async (id) => {
  const summary = await EducationSummary.findById(id);
  return summary;
};

module.exports.create = async (summaryInfo) => {
  const newSummary = new EducationSummary(summaryInfo);
  const savedSummary = await newSummary.save();
  return savedSummary;
};

module.exports.update = async (id, summaryInfo) => {
  const updatedSummary = await EducationSummary.findOneAndUpdate(
    { _id: id },
    summaryInfo,
    {
      new: true,
    }
  );
  return updatedSummary;
};

module.exports.delete = async (id) => {
  const removed = await EducationSummary.findByIdAndRemove(id);
  return removed;
};
