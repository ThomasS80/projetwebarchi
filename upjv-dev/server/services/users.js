const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const User = require("../models/User");

// Register
module.exports.create = async (userData) => {
  // Get user input
  const { email, password } = userData;

  // check user input
  if (!email || !password) {
    return { error: { message: "Missing required fields" } };
  }

  // check if the user already exist
  const existingUser = await User.findOne({ email });
  if (existingUser) {
    return { error: { message: "Adresse mail existe déjà" } };
  }

  // Encrypt password
  const hashPassword = await bcrypt.hash(password, 10);

  // Create a user in our database.
  const user = await User.create({
    email,
    password: hashPassword,
  });

  // Create a signed JWT token
  const token = jwt.sign(
    { userId: user.id, email: user.email, role: user.role },
    process.env.JWT_KEY,
    {
      expiresIn: "1d",
    }
  );

  return {
    userId: user.id,
    username: user.username,
    email: user.email,
    token: "Bearer " + token,
  };
};

// login
module.exports.login = async (loginData) => {
  // Get user credentials
  const { email, password } = loginData;

  // check user credentials
  if (!email || !password) {
    return { error: { message: "Missing required fields" } };
  }

  // check if user exist
  const user = await User.findOne({ email });

  if (!user) {
    return { error: { message: "Login incorrect" } };
  }

  // compare password
  const validPassword = await bcrypt.compare(password, user.password);

  if (!validPassword) {
    return { error: { message: "Mot de passe incorrect" } };
  }

  // Generate token
  const token = jwt.sign(
    {
      userId: user.id,
      email: user.email,
      role: user.role,
    },
    process.env.JWT_KEY,
    { expiresIn: "1d" }
  );

  return {
    email: user.email,
    role: user.role,
    userId: user.id,
    token: "Bearer " + token,
  };
};

// change password
module.exports.changePassword = async (updateInfo) => {
  const { userId, currentPassword, newPassword } = updateInfo;
  const user = await User.findById(userId);

  // compare password
  const validPassword = await bcrypt.compare(currentPassword, user.password);
  if (!validPassword) {
    return { error: { message: "Mot de passe incorrect" } };
  }

  const hashPassword = await bcrypt.hash(newPassword, 10);
  await User.findOneAndUpdate(
    { _id: userId },
    { $set: { password: hashPassword } }
  );

  return { message: "Mot de passe mis à jour" };
};
