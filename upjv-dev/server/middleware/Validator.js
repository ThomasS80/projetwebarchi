const Joi = require("joi");
const { StatusCodes } = require("http-status-codes");

const validators = require("../validators");

module.exports = function (validator) {
  // If validator does not exist, throw err
  if (!validators.hasOwnProperty(validator))
    throw new Error(`'${validator}' validator does not exist`);

  return async function (req, res, next) {
    try {
      const validated = await validators[validator].validateAsync(req.body);
      req.body = validated;
      next();
    } catch (err) {
      //Joi validation errors
      if (err.isJoi)
        return res
          .status(StatusCodes.OK)
          .json({ error: { message: err.message } });
      // other errors
      res
        .status(StatusCodes.INTERNAL_SERVER_ERROR)
        .json({ error: { message: "Internal server error" } });
    }
  };
};
