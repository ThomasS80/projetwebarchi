const { StatusCodes } = require("http-status-codes");
const service = require("../services/professors");
const userService = require("../services/users");

module.exports.getAll = async (req, res) => {
  try {
    const data = await service.findAll();
    res.status(StatusCodes.OK).json(data);
  } catch (err) {
    res
      .status(StatusCodes.INTERNAL_SERVER_ERROR)
      .json({ error: { message: "Internal server error" } });
  }
};

module.exports.getOne = async (req, res) => {
  try {
    const id = req.params.id;
    const data = await service.findById(id);

    res.status(StatusCodes.OK).json(data);
  } catch (err) {
    res
      .status(StatusCodes.INTERNAL_SERVER_ERROR)
      .json({ error: { message: "Internal server error" } });
  }
};

module.exports.add = async (req, res) => {
  try {
    const { email, ...profInfo } = req.body;
    const createdUser = await userService.create({
      email,
      password: process.env.DEFAULT_PASSWORD,
    });

    if (createdUser.error) return res.status(StatusCodes.OK).json(createdUser);

    const createdProf = await service.create({
      user: createdUser.userId,
      ...profInfo,
    });

    res.status(StatusCodes.OK).json(createdProf);
  } catch (err) {
    res
      .status(StatusCodes.INTERNAL_SERVER_ERROR)
      .json({ error: { message: "Internal server error" } });
  }
};

module.exports.update = async (req, res) => {
  try {
    const id = req.params.id;
    const profInfo = req.body;

    const updatedProf = await service.update(id, profInfo);

    res.status(StatusCodes.OK).json(updatedProf);
  } catch (err) {
    res
      .status(StatusCodes.INTERNAL_SERVER_ERROR)
      .json({ error: { message: "Internal server error" } });
  }
};

module.exports.delete = async (req, res) => {
  try {
    const profId = req.params.id;
    const deleted = await service.delete(profId);

    res.status(StatusCodes.OK).json(deleted);
  } catch (error) {
    res
      .status(StatusCodes.INTERNAL_SERVER_ERROR)
      .json({ error: { message: "Internal server error" } });
  }
};
