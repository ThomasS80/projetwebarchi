const { StatusCodes } = require("http-status-codes");
const service = require("../services/users");

// Register
module.exports.register = async (req, res) => {
  try {
    const data = await service.create(req.body);

    res.status(StatusCodes.OK).json(data);
  } catch (err) {
    res
      .status(StatusCodes.INTERNAL_SERVER_ERROR)
      .json({ error: { message: "Internal server error" } });
  }
};

// login
module.exports.login = async (req, res) => {
  try {
    const data = await service.login(req.body);

    // Response
    res.status(StatusCodes.OK).json(data);
  } catch (err) {
    res
      .status(StatusCodes.INTERNAL_SERVER_ERROR)
      .json({ error: { message: "Internal server error" } });
  }
};

// change password
module.exports.changePassword = async (req, res) => {
  try {
    const { userId } = req.user;
    const data = await service.changePassword({ userId, ...req.body });

    // Response
    res.status(StatusCodes.OK).json(data);
  } catch (err) {
    res
      .status(StatusCodes.INTERNAL_SERVER_ERROR)
      .json({ error: { message: "Internal server error" } });
  }
};
