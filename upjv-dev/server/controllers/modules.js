const { StatusCodes } = require("http-status-codes");
const service = require("../services/modules");

module.exports.getAll = async (req, res) => {
  try {
    const data = await service.findAll();

    res.status(StatusCodes.OK).json({ data });
  } catch (err) {
    res
      .status(StatusCodes.INTERNAL_SERVER_ERROR)
      .json({ error: { message: "Internal server error" } });
  }
};

module.exports.getOne = async (req, res) => {
  try {
    const id = req.params.id;
    const data = await service.findById(id);

    res.status(StatusCodes.OK).json({ data });
  } catch (err) {
    res
      .status(StatusCodes.INTERNAL_SERVER_ERROR)
      .json({ error: { message: "Internal server error" } });
  }
};
