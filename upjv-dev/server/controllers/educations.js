const { StatusCodes } = require("http-status-codes");
const service = require("../services/educations");

module.exports.getAll = async (req, res) => {
  try {
    const isProfessor = req.user.role === "professor";
    if (isProfessor) {
      const data = await service.findAllForProfessor(req.user.userId);
      return res.status(StatusCodes.OK).json(data);
    }

    const data = await service.findAll();
    res.status(StatusCodes.OK).json(data);
  } catch (err) {
    res
      .status(StatusCodes.INTERNAL_SERVER_ERROR)
      .json({ error: { message: "Internal server error" } });
  }
};

module.exports.getByModule = async (req, res) => {
  try {
    const id = req.params.id;
    const data = await service.findAllForModule(id);

    res.status(StatusCodes.OK).json(data);
  } catch (err) {
    res
      .status(StatusCodes.INTERNAL_SERVER_ERROR)
      .json({ error: { message: "Internal server error" } });
  }
};

module.exports.getOne = async (req, res) => {
  try {
    const id = req.params.id;
    const data = await service.findById(id);

    res.status(StatusCodes.OK).json(data);
  } catch (err) {
    res
      .status(StatusCodes.INTERNAL_SERVER_ERROR)
      .json({ error: { message: "Internal server error" } });
  }
};

module.exports.add = async (req, res) => {
  try {
    const createdEdu = await service.create(req.body);

    res.status(StatusCodes.OK).json(createdEdu);
  } catch (err) {
    res
      .status(StatusCodes.INTERNAL_SERVER_ERROR)
      .json({ error: { message: "Internal server error" } });
  }
};

module.exports.delete = async (req, res) => {
  try {
    const deletedEdu = await service.delete(req.params.id);

    res.status(StatusCodes.OK).json(deletedEdu);
  } catch (err) {
    console.log(err);
    res
      .status(StatusCodes.INTERNAL_SERVER_ERROR)
      .json({ error: { message: "Internal server error" } });
  }
};
