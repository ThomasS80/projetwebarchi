const mongoose = require("mongoose");

const EducationSummarySchema = new mongoose.Schema(
  {
    educations: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Education",
      },
    ],
    summary: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Module",
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("EducationSummary", EducationSummarySchema);
