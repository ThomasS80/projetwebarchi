const mongoose = require("mongoose");

const EducationSchema = new mongoose.Schema(
  {
    professor: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Professor",
    },
    module: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Module",
    },
    totalGroupsCM: {
      type: Number,
      required: true,
    },
    totalGroupsTD: {
      type: Number,
      required: true,
    },
    totalGroupsTP: {
      type: Number,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Education", EducationSchema);
