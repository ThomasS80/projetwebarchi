const mongoose = require("mongoose");

const UCRuleSchema = new mongoose.Schema(
  {
    status: {
      type: String,
      enum: ["EC", "PRAG", "PAST", "VACATAIRE", "CDE", "ATER"],
      required: true,
    },
    CMFactor: {
      type: Number,
      required: true,
    },
    TDFactor: {
      type: Number,
      required: true,
    },
    TPFactor: {
      type: Number,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("UCRule", UCRuleSchema);
