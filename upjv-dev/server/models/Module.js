const mongoose = require("mongoose");

const BasicTypeSchema = new mongoose.Schema({
  totalHours: Number,
  totalGroups: Number,
  assignedGroups: Number,
});

const ModuleSchema = new mongoose.Schema(
  {
    degree: {
      type: String,
      required: true,
    },
    semester: {
      type: Number,
      required: true,
    },
    ref: {
      type: String,
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      required: true,
    },
    manager: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Professor",
      default: "",
    },
    CM: {
      type: BasicTypeSchema,
      required: true,
    },
    TD: {
      type: BasicTypeSchema,
      required: true,
    },
    TP: {
      type: BasicTypeSchema,
      required: true,
    },
    totalStudents: {
      type: Number,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Module", ModuleSchema);
