const mongoose = require("mongoose");

const ProfessorSchema = new mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      enum: ["EC", "PRAG", "PAST", "VACATAIRE", "CDE", "ATER"],
      required: true,
    },
    minUC: {
      type: Number,
      required: true,
    },
    assignedCMGroups: { type: Number, default: 0 },
    assignedTDGroups: { type: Number, default: 0 },
    assignedTPGroups: { type: Number, default: 0 },
    isManagerOf: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Module",
      },
    ],
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Professor", ProfessorSchema);
