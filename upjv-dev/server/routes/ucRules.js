const { Router } = require("express");
const controller = require("../controllers/ucRules");

const ensureAuth = require("../middleware/ensureAuth");

const router = Router().use(ensureAuth);

router.get("/", controller.getAll);
router.get("/:id", controller.getOne);
router.patch("/:id", controller.update);

module.exports = router;
