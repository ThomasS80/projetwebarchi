const { Router } = require("express");

const controller = require("../controllers/users");
const Validator = require("../middleware/Validator");
const ensureAuth = require("../middleware/ensureAuth");

const router = Router();

// register
router.post("/register", controller.register);

// login
router.post("/login", Validator("login"), controller.login);

// change password
router.post("/change-password", ensureAuth, controller.changePassword);

module.exports = router;
