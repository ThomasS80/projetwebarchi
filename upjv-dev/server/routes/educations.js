const { Router } = require("express");
const controller = require("../controllers/educations");

const Validator = require("../middleware/Validator");
const ensureAuth = require("../middleware/ensureAuth");

const router = Router().use(ensureAuth);

router.get("/", controller.getAll);
router.get("/module/:id", controller.getByModule);
router.get("/:id", controller.getOne);
router.post("/", Validator("education"), controller.add);
router.delete("/:id", controller.delete);

module.exports = router;
