const { Router } = require("express");
const controller = require("../controllers/professors");

const Validator = require("../middleware/Validator");
const ensureAuth = require("../middleware/ensureAuth");

const router = Router().use(ensureAuth);

// get all profs
router.get("/", controller.getAll);

// get prof by id
router.get("/:id", controller.getOne);

// add prof
router.post("/", Validator("professor"), controller.add);

// update prof by id
router.patch("/:id", controller.update);

// delete prof by id
router.delete("/:id", controller.delete);

module.exports = router;
