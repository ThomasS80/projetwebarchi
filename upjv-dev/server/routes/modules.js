const { Router } = require("express");
const controller = require("../controllers/modules");

const ensureAuth = require("../middleware/ensureAuth");

const router = Router().use(ensureAuth);

// get all modules
router.get("/", controller.getAll);

// get module by id
router.get("/:id", controller.getOne);

module.exports = router;
