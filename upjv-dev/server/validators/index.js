const { loginSchema } = require("./auth.validator");
const { professorSchema } = require("./professor.validator");
const { educationSchema } = require("./education.validator");

module.exports = {
  login: loginSchema,
  professor: professorSchema,
  professor: professorSchema,
  education: educationSchema,
};
