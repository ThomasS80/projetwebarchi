const Joi = require("joi");

module.exports.professorSchema = Joi.object({
  email: Joi.string().email().lowercase().required(),
  firstName: Joi.string().required(),
  lastName: Joi.string().required(),
  status: Joi.string().valid("EC", "PRAG", "PAST", "VACATAIRE", "CDE", "ATER"),
  minUC: Joi.number().required(),
});
