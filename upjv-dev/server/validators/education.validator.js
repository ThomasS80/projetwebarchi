const Joi = require("joi");

module.exports.educationSchema = Joi.object({
  moduleId: Joi.string().required(),
  professorId: Joi.string().required(),
  totalGroupsCM: Joi.number().min(0).required(),
  totalGroupsTD: Joi.number().min(0).required(),
  totalGroupsTP: Joi.number().min(0).required(),
});
